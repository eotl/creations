Midori Notebooks
================

Printing templates to create various [Midori Travelers Notebooks](https://www.thejournalshop.com/notebooks/by-brand/midori/travelers-notebook)
patterns that can be printed locally and zero-waste.
