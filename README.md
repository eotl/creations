# Creations

A place for tangible creations, public domain products, art, etc.

## Mouthy

A simple, futuristic stylish, tootbrush stand for 3D ceramic printing.

![Mouthy WIP](https://codeberg.org/eotl/creations/raw/branch/main/mouthy/mouthy-first-print-2.jpg)


## Water Sprout


A geometric ceramic 3D printed reimagining of the Olla Irrigation pot.

![Water Sprout WIP](https://codeberg.org/eotl/creations/raw/branch/main/water-sprout/water-sprout-1st-print.jpg)


## Toughie Bag

A reusable, durable, waxed canvas bag fit for multi-purposes delivery, sale, storage.

![Toughie Bag Test](https://codeberg.org/eotl/creations/raw/branch/main/toughie-bag/toughie-bag-test.jpg)
