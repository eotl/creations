Water Sprout
============

A futuristic design update for the traditional [Olla irrigation](https://en.wikipedia.org/wiki/Olla#Use_in_irrigation) which
maximizes soil penetration and is meant to be ceramic 3D printed. Conceived by
EOTL then designed and printed by [Daniel Valencia Ferrá](https://www.digitalcraftdvf.com/danielvalenciaferra).
